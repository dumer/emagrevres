FROM node:9.11-alpine

WORKDIR /srv/

EXPOSE 22

ENTRYPOINT npm install && npm start
