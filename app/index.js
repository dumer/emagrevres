let config = require('./config/config');
let BuilderContainer = require('./src/builder/Container');
let AccomplishContainer = require('./src/accomplish/Container');
let ClusterContainer = require('./src/cluster/Container');
let StreamContainer = require('./src/stream/Container');

let builderContainer = new BuilderContainer();
let clusterContainer = new ClusterContainer();
let accomplishContainer = new AccomplishContainer();
let streamContainer = new StreamContainer();

let cluster = clusterContainer.get('Cluster');
cluster.setInterval(config.cluster.server.interval);

let clusterBuilder = builderContainer.get('ClusterBuilder');
clusterBuilder.build(cluster);

cluster.start();

let accomplish = accomplishContainer.get('Accomplish');
accomplish.setInterval(config.accomplish.interval);
accomplish.start(cluster);

let stream = streamContainer.get('Stream');
stream.setListen(config.stream.port);
stream.start(cluster);

// 1#login#password
// 2#hash#1#auth
// 3#login#password#auth
