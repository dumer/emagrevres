let Matrix = require('./Matrix');

class Remove extends Matrix {

    process(matriz, matriz_remove, x, y) {

        let matriz_remove_largura = matriz_remove[0].length;
        let matriz_remove_altura = matriz_remove.length;

        for (let i = 0; i < matriz_remove_altura; i++) {
            for (let j = 0; j < matriz_remove_largura; j++) {
                if (matriz_remove[i][j]) {
                    matriz[i + y][j + x] = false;
                }
            }
        }

        return matriz;
    }
}

module.exports = Remove;
