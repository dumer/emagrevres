let Matrix = require('./Matrix');

class Cut extends Matrix {

    process(matriz, x, y, w, h) {

    	let matriz_r = [];

    	for (let i = 0; i < w; i++) {

            if (matriz[y + i]) {

                matriz_r[i] = [];

                for (let j = 0; j < h; j++) {
                    matriz_r[i][j] = matriz[y + i][x + j];
                }
            }
    	}

    	return matriz_r;
    }
}

module.exports = Cut;
