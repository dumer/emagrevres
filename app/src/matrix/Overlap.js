let Matrix = require('./Matrix');

class Overlap extends Matrix {

    process(matriz, matriz_add) {

        let matriz_add_largura = matriz_add[0].length;
        let matriz_add_altura = matriz_add.length;
        let b = false;

        for (let i = 0; i < matriz_add_altura; i++) {
            for (let j = 0; j < matriz_add_largura; j++) {
                if ((matriz_add[i][j] == true)
                    && (matriz[i] === undefined ||
                        matriz[i][j] === undefined ||
                        matriz[i][j] == true ||
                        matriz[i][j] == null)
                ) {
                    b = true;
                }
            }
        }

        return b;
    }
}

module.exports = Overlap;
