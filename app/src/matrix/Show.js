let Matrix = require('./Matrix');

class Show extends Matrix {

    process(matriz) {

        let ms = '';

        matriz.forEach(function(valor, i) {

            matriz[i].forEach(function(valor, j) {

                if (matriz[i][j]) {
                    ms += ' T';
                } else {
                    ms += ' N';
                }
            });

            ms += '\n';
        });

        console.log(ms);
    }
}

module.exports = Show;
