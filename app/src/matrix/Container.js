let AbstractContainer = require('../library/Container');
let Cut = require('./Cut');
let Merge = require('./Merge');
let Overlap = require('./Overlap');
let Remove = require('./Remove');
let Show = require('./Show');

class Container extends AbstractContainer {

    set() {
        return {
            Cut: function(container) {
                return new Cut();
            },
            Merge: function(container) {
                return new Merge();
            },
            Overlap: function(container) {
                return new Overlap();
            },
            Remove: function(container) {
                return new Remove();
            },
            Show: function(container) {
                return new Show();
            }
        }
    }
}

module.exports = Container;
