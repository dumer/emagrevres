let Matrix = require('./Matrix');

class Merge extends Matrix {

    process(matriz, matriz_add, x, y) {

        let matriz_add_largura = matriz_add[0].length;
        let matriz_add_altura = matriz_add.length;

        for (let i = 0; i < matriz_add_altura; i++) {
            for (let j = 0; j < matriz_add_largura; j++) {
                if (matriz_add[i][j]) {
                    matriz[i + y][j + x] = true;
                }
            }
        }

        return matriz;
    }
}

module.exports = Merge;
