class Builder {

    constructor(container) {
        this.container = container;
    }

    getContainer() {
        return this.container;
    }

    setContainer(container) {
        this.container = container;
    }

    process(o) {
        return o;
    }

    build(o) {
        return this.process(o);
    }
}

module.exports = Builder;
