let AbstractContainer = require('../library/Container');
let ClusterBuilder = require('./cluster/ClusterBuilder');
let ServerBuilder = require('./server/ServerBuilder');

class Container extends AbstractContainer {

    set() {
        return {
            DimensionContainer: function(container) {
                return new DimensionContainer();
            },
            ClusterBuilder: function(container) {
                return new ClusterBuilder(container);
            },
            ServerBuilder: function(container) {
                return new ServerBuilder(container);
            }
        }
    }
}

module.exports = Container;
