class Map {

    constructor(tamanho) {

        this.tamanho = tamanho;
        this.array = [];

        this.geraMatriz();
    }

    getTamanho() {
        return this.tamanho;
    }

    getArray() {
        return this.array;
    }

    setTamanho(tamanho) {
        this.tamanho = tamanho;
    }

    setArray(array) {
        this.array = array;
    }

    geraMatriz() {

        for (let i = 0; i < this.tamanho; i++) {

            this.array[i] = [];

            for (let j = 0; j < this.tamanho; j++) {
                this.array[i][j] = false;
            }
        }
    }
}

module.exports = Map;
