let CortaMatriz = require('./matriz/CortaMatriz');
let MesclaMatriz = require('./matriz/MesclaMatriz');
let RemoveMatriz = require('./matriz/RemoveMatriz');
let SobrepoeMatriz = require('./matriz/SobrepoeMatriz');

class EsquerdaMoveCoisaMap {

    constructor() {

        this.cm = new CortaMatriz();
        this.mm = new MesclaMatriz();
        this.rm = new RemoveMatriz();
        this.sm = new SobrepoeMatriz();
    }

    mover(map, coisa, x, y, z) {

        let m = this.cm.cortar(
            map.getArray(),
            x - z, y,
            coisa.getFormaGeometrica().getWidth() + z,
            coisa.getFormaGeometrica().getHeight());

        this.rm.remover(m, coisa.getFormaGeometrica().getArray(), z, 0);

        if (!this.sm.sobrepor(m, coisa.getFormaGeometrica().getArray())) {

            this.rm.remover(map.getArray(), coisa.getFormaGeometrica().getArray(), x, y);
            this.mm.mesclar(map.getArray(), coisa.getFormaGeometrica().getArray(), x - z, y);
        }
    }
}

module.exports = EsquerdaMoveCoisaMap;
