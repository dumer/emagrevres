let CortaMatriz = require('./matriz/CortaMatriz');
let MesclaMatriz = require('./matriz/MesclaMatriz');
let SobrepoeMatriz = require('./matriz/SobrepoeMatriz');

class AdicionaCoisaMap {

    constructor() {

        this.cm = new CortaMatriz();
        this.mm = new MesclaMatriz();
        this.sm = new SobrepoeMatriz();
    }

    adicionar(map, coisa, x, y) {

        let m = this.cm.cortar(map.getArray(), x, y, coisa.getFormaGeometrica().getWidth(), coisa.getFormaGeometrica().getHeight());

        if (!this.sm.sobrepor(m, coisa.getFormaGeometrica().getArray())) {
            this.mm.mesclar(map.getArray(), coisa.getFormaGeometrica().getArray(), x, y);
        }
    }
}

module.exports = AdicionaCoisaMap;
