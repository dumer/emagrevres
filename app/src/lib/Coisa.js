class Coisa {

    constructor(formaGeometrica) {
        this.formaGeometrica = formaGeometrica;
    }

    getFormaGeometrica() {
        return this.formaGeometrica;
    }

    setFormaGeometrica(formaGeometrica) {
        this.formaGeometrica = formaGeometrica;
    }
}

module.exports = Coisa;
