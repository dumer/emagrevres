let FormaGeometrica = require('./FormaGeometrica');

class Circulo extends FormaGeometrica {

    constructor(lado) {

        super(lado * 2, lado * 2);

        this.lado = lado;
    }

    getLado() {
        return this.lado;
    }

    setLado(lado) {
        this.lado = lado;
    }

    getArray() {

        let matrizArea = [];

        for (let i = 0; i < this.lado * 2; i++) {

            matrizArea[i] = [];

            for (let j = 0; j < this.lado * 2; j++) {
                matrizArea[i][j] = false;
            }
        }

        let x0 = this.lado;
        let y0 = this.lado;
        let radius = this.lado;

        let x = radius-1;
        let y = 0;
        let dx = 1;
        let dy = 1;
        let err = dx - (radius << 1);

        while (x >= y) {

            matrizArea[x0 + x][y0 + y] = true;
            matrizArea[x0 + y][y0 + x] = true;
            matrizArea[x0 - y][y0 + x] = true;
            matrizArea[x0 - x][y0 + y] = true;
            matrizArea[x0 - x][y0 - y] = true;
            matrizArea[x0 - y][y0 - x] = true;
            matrizArea[x0 + y][y0 - x] = true;
            matrizArea[x0 + x][y0 - y] = true;

            if (err <= 0) {

                y++;
                dy += 2;
                err += dy;
            }

            if (err > 0) {

                x--;
                dx += 2;
                err += dx - (radius << 1);
            }
        }

        // move para cima e para a esquerda
        for (let i = 1; i < this.lado * 2; i++) {
            for (let j = 1; j < this.lado * 2; j++) {
                matrizArea[i - 1][j - 1] = matrizArea[i][j];
            }
        }

        // move parte inferior e direita para o lado
        for (let i = this.lado * 2 - 1; i > this.lado; i--) {
            for (let j = 0; j < this.lado * 2; j++) {
                matrizArea[i][j] = matrizArea[i - 1][j];
                matrizArea[j][i] = matrizArea[j][i - 1];
            }
        }

        // preenche interior do circulo
        let interior = false;

        for (let i = 1; i < this.lado * 2 - 1; i++) {

            interior = true;

            if (!matrizArea[i][0]) {
                interior = false;
            }

            for (let j = 1; j < this.lado * 2 - 1; j++) {

                if (!interior && matrizArea[i][j] && !matrizArea[i][j + 1]) {
                    interior = true;
                } else if (interior) {

                    matrizArea[i][j] = true;

                    if (matrizArea[i][j + 1]) {
                        break;
                    }
                }
            }
        }

        return matrizArea;
    }
}

module.exports = Circulo;
