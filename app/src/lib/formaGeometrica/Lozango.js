let FormaGeometrica = require('./FormaGeometrica');

class Lozango extends FormaGeometrica {

    constructor(lado) {

        super(lado, lado);

        this.lado = lado;
    }

    getLado() {
        return this.lado;
    }

    setLado(lado) {
        this.lado = lado;
    }

    getArray() {

        let matrizArea = [];
        let espaco = parseInt(this.lado / 2);
        let espaco_linha = this.lado % 2;
        let cima = true;
        let meio_par = false;

        if (espaco_linha == 0) {
            espaco -= 1;
            espaco_linha += 2;
        }

        for (let i = 0; i < this.lado; i++) {

            matrizArea[i] = [];

            for (let j = 0; j < this.lado; j++) {

                matrizArea[i][j] = false;

                if ((j + 1) > espaco && (j + 1) <= (espaco + espaco_linha)) {
                    matrizArea[i][j] = true;
                }
            }

            if (cima) {

                espaco -= 1;
                espaco_linha += 2;

                if (espaco_linha == this.lado) {
                    cima = false;

                    if (this.lado % 2 == 0) {
                        meio_par = true;
                    }
                }

            } else if (meio_par) {
                meio_par = false;
            }else {
                espaco += 1;
                espaco_linha -= 2;
            }
        }

        return matrizArea;
    }
}

module.exports = Lozango;
