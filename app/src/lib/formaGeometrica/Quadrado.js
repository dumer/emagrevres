let FormaGeometrica = require('./FormaGeometrica');

class Quadrado extends FormaGeometrica {

    constructor(lado) {

        super(lado, lado);

        this.lado = lado;
    }

    getLado() {
        return this.lado;
    }

    setLado(lado) {
        this.lado = lado;
    }

    getArray() {

        let matrizArea = [];

        for (let i = 0; i < this.lado; i++) {

            matrizArea[i] = [];

            for (let j = 0; j < this.lado; j++) {
                matrizArea[i][j] = true;
            }
        }

        return matrizArea;
    }
}

module.exports = Quadrado;
