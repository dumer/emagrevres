let FormaGeometrica = require('./FormaGeometrica');

class Retangulo extends FormaGeometrica {

    constructor(largura, altura) {

        super(largura, altura);

        this.largura = largura;
        this.altura = altura;
    }

    getLargura() {
        return this.largura;
    }

    getAltura() {
        return this.altura;
    }

    setLargura(largura) {
        this.largura = largura;
    }

    setAltura() {
        this.altura = altura;
    }

    getArray() {

        let matrizArea = [];

        for (let i = 0; i < this.largura; i++) {

            matrizArea[i] = [];

            for (let j = 0; j < this.altura; j++) {
                matrizArea[i][j] = true;
            }
        }

        return matrizArea;
    }
}

module.exports = Retangulo;
