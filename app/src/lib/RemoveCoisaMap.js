let RemoveMatriz = require('./matriz/RemoveMatriz');

class RemoveCoisaMap {

    constructor() {
        this.rm = new RemoveMatriz();
    }

    remover(map, coisa, x, y) {
        this.rm.remover(map.getArray(), coisa.getFormaGeometrica().getArray(), x, y);
    }
}

module.exports = RemoveCoisaMap;
