class Accomplish {

    constructor() {
        this.interval;
    }

    getInterval() {
        return this.interval;
    }

    setInterval(interval) {
        this.interval = interval;
    }

    loop(cluster, time) {
        // console.log(cluster, time);
    }

    start(cluster) {

        let self = this;

        setInterval(function() {

            self.loop(cluster, Date.now());

        }, this.interval);
    }
}

module.exports = Accomplish;
