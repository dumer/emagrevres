let AbstractContainer = require('../library/Container');
let Accomplish = require('./Accomplish');

class Container extends AbstractContainer {

    set() {
        return {
            Accomplish: function(container) {
                return new Accomplish();
            }
        }
    }
}

module.exports = Container;
