let AbstractContainer = require('../library/Container');
let Net = require('net');
let Stream = require('./Stream');
let Request = require('./Request');

class Container extends AbstractContainer {

    set() {
        return {
            Socket: function(container) {
                return Net.createServer();
            },
            Stream: function(container) {

                let socket = container.get('Socket');

                return new Stream(container, socket);
            },
            Request: function(container) {
                return new Request();
            }
        }
    }
}

module.exports = Container;
