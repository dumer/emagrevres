class Request {

    constructor() {
        this.data;
    }

    getData() {
        return this.data;
    }

    setData(data) {
        this.data = data;
    }
}

module.exports = Request;
