let Container = require('./Container');

class Stream {

    constructor(container, socket) {

        this.container = container;
        this.socket = socket;
    }

    setListen(port) {
        this.socket.listen(port);
    }

    start(cluster) {

        this.socket.on('connection', function(connection) {

            connection.on('data', function(data) {

                let request = this.container.get('Request');

                request.setData(data);

                connection.write('');

                // console.log(data.toString());
                //
                // switch (data.toString()) {
                //     case "up": connection.write('up'); break;
                //     case "left": connection.write('left'); break;
                //     case "right": connection.write('right'); break;
                //     case "down": connection.write('down'); break;
                //     default: connection.write('');
                // }
            })

            connection.on('close', function (close) {

            });
        });
    }
}

module.exports = Stream;
