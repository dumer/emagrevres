let AbstractContainer = require('../library/Container');
let Dimension = require('./Dimension');

class Container extends AbstractContainer {

    set() {
        return {
            Dimension: function() {
                return new Dimension();
            }
        }
    }
}

module.exports = Container;
