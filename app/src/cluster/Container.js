let AbstractContainer = require('../library/Container');
let Cluster = require('./Cluster');

class Container extends AbstractContainer {

    set() {
        return {
            Cluster: function(container) {
                return new Cluster();
            }
        }
    }
}

module.exports = Container;
