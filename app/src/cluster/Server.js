class Server {

    constructor(id, interval) {

        this.id = id;
        this.interval = interval;
        this.dimension;
    }

    getId() {
        return this.id;
    }

    getInterval() {
        return this.interval;
    }

    getDimension() {
        return this.dimension;
    }

    setId(id) {
        this.id = id;
    }

    setInterval(interval) {
        this.interval = interval;
    }

    setDimension(dimension) {
        this.dimension = dimension;
    }

    start() {
        
        this.dimension.start();

        let self = this;

        setInterval((function() {

            self.loop(Date.now());

        }), this.interval);
    }

    loop(time) {

        console.debug('SERVER '+this.id+' -> '+time);
    }
}

module.exports = Server;
