class Cluster {

    constructor() {
        this.interval;
        this.servers = [];
    }

    getInterval() {
        return this.interval;
    }

    getServers() {
        return this.servers;
    }

    setInterval(interval) {
        this.interval = interval;
    }

    setServers(servers) {
        this.servers = servers;
    }

    start() {

        let self = this;

        this.servers.forEach(function(server, index) {
            self.servers[index].start(this.interval);
        });
    }

    getServerByIndex(index) {
        return this.servers[index];
    }

    getServerById(id) {

        let self = this;

        this.servers.forEach(function(server, index) {

            if (self.server[index].getId() == id) {
                return self.server[index];
            }
        });
    }

    addServer(server) {
        this.servers.push(server);
    }
}

module.exports = Cluster;
